<!DOCTYPE html>
<html lang="en">
<head>
    <title>Core Dream Innovation | Task</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{csrf_token()}}">
    <link rel="stylesheet" href="{{url('public/assets/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{url('public/assets/css/jquery-ui.min.css')}}">
    <link rel="stylesheet" href="{{url('public/assets/css/validationEngine.jquery.css')}}" type="text/css"/>
    <link rel="stylesheet" href="{{url('public/assets/css/datatable.min.css')}}" type="text/css"/>
</head>
<body>
<style>
    ::-webkit-input-placeholder {
        text-transform: initial;
    }

    :-moz-placeholder {
        text-transform: initial;
    }

    ::-moz-placeholder {
        text-transform: initial;
    }

    :-ms-input-placeholder {
        text-transform: initial;
    }
</style>
<div class="container">
    @if(Session::has('flash_message'))
        <div class="alert alert-success">
            <strong>Success!</strong> {{ Session::get('flash_message') }}
        </div>
    @endif
    @yield('content')
</div>
<script src="{{url('public/assets/js/jquery-3.2.1.min.js')}}"></script>
<script src="{{url('public/assets/js/bootstrap.min.js')}}"></script>
<script src="{{url('public/assets/js/jquery.validationEngine-en.js')}}" type="text/javascript" charset="utf-8"></script>
<script src="{{url('public/assets/js/jquery.validationEngine.js')}}" type="text/javascript" charset="utf-8"></script>
<script src="{{url('public/assets/js/jquery-ui.min.js')}}"></script>
<script src="{{url('public/assets/js/datatable.min.js')}}"></script>
<script src="{{url('public/assets/js/jquery.mask.min.js')}}"></script>
<script src="{{url('public/assets/js/Shared.js')}}"></script>
@yield('scripts')
</body>
</html>
