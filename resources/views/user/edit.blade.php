@extends('master')

@section('content')
    @php($add = 1)
    @if(isset($data)!=0)
        @php($add = 0)
    @endif
<style>
    body
    {
        background-color: gray;
    }
</style>
    <div class="card">
    <div class="container col-md-4 col-md-offset-4" style="background: white;border-radius: 4px;">
        <h2>User Detail Form</h2>
        <form action="{{url('user/update')}}" autocomplete="off" method="post" id="user-form">
            {{ csrf_field() }}
            <div class="form-group">
                <label for="name">Name:</label>
                <input type="text" class="form-control validate[required] text-capitalize" id="fullname"
                       placeholder="Enter name" name="fullname" value="{{!$add ? $data['Full Name']:''}}">
            </div>
            <div class="form-group">
                <label for="gender">Gender:</label>
                <div class="radio">
                    <label><input type="radio" name="gender" value="male"
                                  @if(!$add ? $data['Gender']=='male':"")checked @endif>Male</label>
                    <label><input type="radio" name="gender" value="female"
                                  @if(!$add ? $data['Gender']=='female':"")checked @endif>Female</label>
                </div>
            </div>
            <div class="form-group">
                <label for="phone">Phone:</label>
                <input type="text" class="form-control validate[required,minSize[10],maxSize[15]]" id="phone"
                       placeholder="Enter phone" name="phone" maxlength="15" value="{{!$add ? $data['Phone']:''}}">
            </div>
            <div class="form-group">
                <label for="email">Email:</label>
                <input type="text" class="form-control validate[required,custom[email]] text-lowercase" id="email"
                       placeholder="Enter email" name="email" value="{{!$add ? $data['Email']:''}}">
            </div>
            <div class="form-group">
                <label for="address">Address:</label>
                <textarea class="form-control validate[required] text-capitalize" id="address"
                          placeholder="Enter address" name="address">{{!$add ? $data['Address']:''}}</textarea>
            </div>
            <div class="form-group">
                <label for="nationality">Nationality:</label>
                <input type="text" class="form-control validate[required,maxSize] text-capitalize" id="nationality"
                       placeholder="Enter nationality" name="nationality" maxlength="20"
                       value="{{!$add ? $data['Nationality']:''}}">
            </div>
            <div class="form-group">
                <label for="dob">Date of Birth:</label>
                <input type="text" class="form-control validate[required,custom[date]]" id="dob"
                       placeholder="Date Of Birth" name="dob" value="{{!$add ? $data['Date of Birth']:''}}">
            </div>
            <div class="form-group">
                <label for="educationBackground">Education Background:</label>
                <select class="form-control validate[required]" id="educationBackground" name="education_background">
                    <option value="">Select Education Background</option>
                    <option @if(!$add ? $data['Education Background'] == "SLC":"")selected @endif value="SLC">SLC
                    </option>
                    <option @if(!$add ? $data['Education Background'] == "HSEB":"")selected @endif value="HSEB">HSEB
                    </option>
                    <option @if(!$add ? $data['Education Background'] == "Bachelor":"")selected @endif value="Bachelor">
                        Bachelor
                    </option>
                    <option @if(!$add ? $data['Education Background'] == "Masters":"")selected @endif value="Masters">
                        Masters
                    </option>
                </select>
            </div>
            <div class="form-group">
                <label for="preferredContact">Preferred Contact:</label>
                <select class="form-control validate[required]" id="preferredContact" name="preferred_contact">
                    <option value="">Select Preferred Contact</option>
                    <option @if(!$add ? $data['Preferred Contact'] == "Email":"")selected @endif value="Email"
                            id="preferredEmail">Email
                    </option>
                    <option @if(!$add ? $data['Preferred Contact'] == "Phone":"")selected @endif value="Phone"
                            id="preferredPhone">Phone
                    </option>
                </select>
            </div>
            <div class="col-md-12" style="margin-top: 20px;">
                <div class="col-sm-12 dialog-button">
                    <input type="submit" value="Update" class="btn btn-rounded btn-success">
                </div>
            </div>
        </form>
    </div>
    </div>
@stop

@section('scripts')
    <script>

        $(function () {
            $("#user-form").validationEngine();
            $('#dob').datepicker({
                dateFormat: '' +
                'yy-mm-dd'
            });

            $('#phone').mask('000-0000-000');
        });


    </script>
@stop
