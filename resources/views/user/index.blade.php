@extends('master')

@section('content')

    <div class="container">
        <h2>User Details</h2>
        <button class="btn btn-success pull-right" style="margin-bottom: 10px;" onclick="location.href='{{url('/user/create')}}'">
            <i class="glyphicon glyphicon-plus"></i> Add New</button>
        <table id="user-list" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
            <tr>
                <th>Name</th>
                <th>Gender</th>
                <th>Email</th>
                <th>Phone</th>
                <th>Address</th>
                <th>Nationality</th>
                <th>DOB</th>
                <th>Education</th>
                <th>Preferred Contact</th>
                <th width="120px">Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($users as $records)
                <tr>
                    <td>{{$records['Full Name']}}</td>
                    <td class="text-capitalize">{{$records['Gender']}}</td>
                    <td>{{$records['Email']}}</td>
                    <td>{{$records['Phone']}}</td>
                    <td>{{$records['Address']}}</td>
                    <td>{{$records['Nationality']}}</td>
                    <td>{{$records['Date of Birth']}}</td>
                    <td>{{$records['Education Background']}}</td>
                    <td>{{$records['Preferred Contact']}}</td>
                    <td>
                        <a href="{{ url('user/'.base64_encode($records['Email']).'/edit') }}" class="btn btn-default">Edit</a>
                        <a onclick="return confirm('Are you sure want to do this?')" href="{{ url('user/'.base64_encode($records['Email']).'/delete') }}" class="btn btn-danger">Delete</a>
                    </td>

                </tr>
            @endforeach
            </tbody>
        </table>

    </div>
@stop

@section('scripts')
    <script>
        $(document).ready(function () {
            $('#user-list').DataTable();
        });

    </script>
@stop