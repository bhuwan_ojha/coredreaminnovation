@extends('master')

@section('content')
    <style>
        body
        {
            background-color: gray;
        }
    </style>

    <div class="card">
        <div class="container col-md-4 col-md-offset-4" style="background: white;border-radius: 4px;">
            <h2>User Detail Form</h2>

            <form action="{{url('user/store')}}" autocomplete="off" method="post" id="user-form">
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="name">Name:</label>
                    <input type="text" class="form-control validate[required] text-capitalize" id="fullname"
                           placeholder="Enter name" name="fullname" value="">
                </div>
                <div class="form-group">
                    <label for="gender">Gender:</label>
                    <div class="radio">
                        <label><input type="radio" name="gender" value="male" checked>Male</label>
                        <label><input type="radio" name="gender" value="female">Female</label>
                    </div>
                </div>
                <div class="form-group">
                    <label for="phone">Phone:</label>
                    <input type="text" class="form-control validate[required,minSize[10],maxSize[15]]" id="phone"
                           placeholder="Enter phone" name="phone" maxlength="15" value="">
                </div>
                <div class="form-group">
                    <label for="email">Email:</label>
                    <input type="text" class="form-control validate[required,custom[email]] text-lowercase" id="email"
                           placeholder="Enter email" name="email" value="">
                </div>
                <div class="form-group">
                    <label for="address">Address:</label>
                    <textarea class="form-control validate[required] text-capitalize" id="address"
                              placeholder="Enter address" name="address"></textarea>
                </div>
                <div class="form-group">
                    <label for="nationality">Nationality:</label>
                    <input type="text" class="form-control validate[required,maxSize] text-capitalize" id="nationality"
                           placeholder="Enter nationality" name="nationality" maxlength="20"
                           value="">
                </div>
                <div class="form-group">
                    <label for="dob">Date of Birth:</label>
                    <input type="text" class="form-control validate[required,custom[date]]" id="dob"
                           placeholder="Date Of Birth" name="dob" value="">
                </div>
                <div class="form-group">
                    <label for="educationBackground">Education Background:</label>
                    <select class="form-control validate[required]" id="educationBackground" name="education_background">
                        <option value="">Select Education Background</option>
                        <option value="SLC">SLC
                        </option>
                        <option value="HSEB">HSEB
                        </option>
                        <option  value="Bachelor">
                            Bachelor
                        </option>
                        <option value="Masters">
                            Masters
                        </option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="preferredContact">Preferred Contact:</label>
                    <select class="form-control validate[required]" id="preferredContact" name="preferred_contact">
                        <option value="">Select Preferred Contact</option>
                        <option value="Email"
                                id="preferredEmail">Email
                        </option>
                        <option value="Phone"
                                id="preferredPhone">Phone
                        </option>
                    </select>
                </div>
                <button type="submit" class="btn btn-success">Save</button>
            </form>
        </div>
    </div>
@stop

@section('scripts')
    <script>


        $(function () {
            $("#user-form").validationEngine();
            $('#dob').datepicker({
                dateFormat: 'yy-mm-dd'
            });
            $('#dob').mask('YYYY-MM-DD');
            $('#phone').mask('000-0000-000');
        });


    </script>
@stop
