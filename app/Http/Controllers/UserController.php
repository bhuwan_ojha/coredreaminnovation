<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class UserController extends Controller
{

    public function index()
    {
        $users = $this->getFileData();
        return view('user.index', compact('users'));

    }

    /* Extract data from csv file*/
    public function getFileData()
    {
        $file = fopen("file.csv", "r");

        $header = fgetcsv($file);


        $data = array();
        while ($row = fgetcsv($file)) {
            $arr = array();
            foreach ($header as $i => $col)
                $arr[$col] = $row[$i];
            $data[] = $arr;
        }
        return $data;
    }

    //store form data into csv file
    public function store(Request $request)
    {
        $this->validate($request, [
            'fullname' => 'required|max:255',
            'phone' => 'required|max:15|min:10',
            'email' => 'required|max:255',
            'address' => 'required',
            'nationality' => 'required|max:20',
            'dob' => 'required',
            'education_background' => 'required',
            'preferred_contact' => 'required',
        ]);

        $input = $request->all();

        if (!file_exists('file.csv')) {
            $fp = fopen('file.csv', 'w');
            fputcsv($fp, array('ID', 'Full Name', 'Gender', 'Phone', 'Email', 'Address', 'Nationality', 'Date of Birth', 'Education Background', 'Preferred Contact'));
        } else {
            $fp = fopen('file.csv', 'a');
        }
        fputcsv($fp, $input);

        fclose($fp);

        $request->session()->flash('flash_message', 'User is successfully added!');
        return redirect('users');

    }

    //update particular user in csv
    public function update(Request $request)
    {
        $this->validate($request, [
            'fullname' => 'required|max:255',
            'phone' => 'required|max:15|min:10',
            'email' => 'required|max:255',
            'address' => 'required',
            'nationality' => 'required|max:20',
            'dob' => 'required',
            'education_background' => 'required',
            'preferred_contact' => 'required',
        ]);

        $data = $request->all();

        $fileData = $this->getFileData();
// Search
        $dataPosition = array_search($data['email'], array_column($fileData, 'Email'));
// Remove from array
        unset($fileData[$dataPosition]);


        $fp = fopen('file.csv', 'w');
        fputcsv($fp, array('ID', 'Full Name', 'Gender', 'Phone', 'Email', 'Address', 'Nationality', 'Date of Birth', 'Education Background', 'Preferred Contact'));
        foreach ($fileData as $value) {
            fputcsv($fp, $value);
        }
        $fp = fopen('file.csv', 'a');
        fputcsv($fp, $data);

        $request->session()->flash('flash_message', 'User updated successfully!');

        return redirect('/users');

    }

    //delete users row in csv
    public function delete($email)
    {
        $fileData = $this->getFileData();

        $dataPosition = array_search(base64_decode($email), array_column($fileData, 'Email'));

        unset($fileData[$dataPosition]);

        $fp = fopen('file.csv', 'w');
        fputcsv($fp, array('ID', 'Full Name', 'Gender', 'Phone', 'Email', 'Address', 'Nationality', 'Date of Birth', 'Education Background', 'Preferred Contact'));
        foreach ($fileData as $value) {
            fputcsv($fp, $value);
        }
        fclose($fp);

        Session::flash('flash_message', 'User deleted successfully!');
        return redirect('users');

    }

    //view addd user form this function
    public function create()
    {
        return view('user.create');
    }

    //edit user form calling through this function
    public function edit($email)
    {
        $fileData = $this->getFileData();
        $dataPosition = array_search(base64_decode($email), array_column($fileData, 'Email'));
        return view('user.edit',['data'=>$fileData[$dataPosition]]);
    }

}
